using UnityEngine;

namespace Domain
{
    public class Configuration
    {
        public static int GridWidth = 3;
        public static int GridHeight = 4;
        public static float ObstacleProbability = .2f;
        public static Vector2Int StartPosition = Vector2Int.zero;
    }
}