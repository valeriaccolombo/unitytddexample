namespace Domain
{
    public enum TerrainType
    {
        START = 0,
        GRASS,
        TREE,
        FINISH
    }
}