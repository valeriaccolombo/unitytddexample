using System.Collections.Generic;
using UnityEngine;

namespace Domain
{
    public class MapBuilder
    {
        public virtual Vector2Int GetStartPosition()
        {
            return Configuration.StartPosition;
        }

        public virtual List<List<TerrainType>> GenerateMap()
        {
            var map = new List<List<TerrainType>>();

            for (var width = 0; width < Configuration.GridWidth; width++)
            {
                var row = new List<TerrainType>();
                for (var height = 0; height < Configuration.GridHeight; height++)
                {
                    if(Random.Range(0f, 1f) <= Configuration.ObstacleProbability)
                        row.Add(TerrainType.TREE);
                    else
                        row.Add(TerrainType.GRASS);
                }
                map.Add(row);
            }
            map[Configuration.StartPosition.x][Configuration.StartPosition.y] = TerrainType.START;
            map[Configuration.GridWidth-1][Configuration.GridHeight-1] = TerrainType.FINISH;
            
            return map;
        }
    }
}