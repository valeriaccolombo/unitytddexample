using System.Collections.Generic;
using Domain;
using UnityEngine;

namespace Delivery
{
    public interface IGameplayView
    {
        void InitializeMap(List<List<TerrainType>> map, Vector2Int characterPosition);
        void MovePlayerToCell(int row, int column);
        void ShowWinFeedback();
    }
}