using System.Collections.Generic;
using Delivery;
using Domain;
using UnityEngine;

namespace Presentation
{
    public class GameplayPresenter
    {
        public Vector2Int CharacaterPosition { get; private set; }
        private List<List<TerrainType>> map;
        private IGameplayView gameplayView;
        
        public GameplayPresenter(IGameplayView gameplayView, MapBuilder mapBuilder)
        {
            map = mapBuilder.GenerateMap();
            CharacaterPosition = mapBuilder.GetStartPosition();
            this.gameplayView = gameplayView;

            gameplayView.InitializeMap(map, CharacaterPosition);
        }

        public void MoveCharacterRight()
        {
            if (IsValidPosition(CharacaterPosition.x + 1, CharacaterPosition.y))
            {
                MoveCharacterToPosition(CharacaterPosition.x + 1, CharacaterPosition.y);
            }
        }
        
        public void MoveCharacterLeft()
        {
            if(IsValidPosition(CharacaterPosition.x-1, CharacaterPosition.y))
            {
                MoveCharacterToPosition(CharacaterPosition.x-1, CharacaterPosition.y);
            }
        }

        public void MoveCharacterUp()
        {
            if(IsValidPosition(CharacaterPosition.x, CharacaterPosition.y+1))
            {
                MoveCharacterToPosition(CharacaterPosition.x, CharacaterPosition.y+1);
            }
        }

        public void MoveCharacterDown()
        {
            if(IsValidPosition(CharacaterPosition.x, CharacaterPosition.y-1))
            {
                MoveCharacterToPosition(CharacaterPosition.x, CharacaterPosition.y-1);
            }
        }

        private void MoveCharacterToPosition(int newX, int newY)
        {
            CharacaterPosition = new Vector2Int(newX, newY);
            gameplayView.MovePlayerToCell(CharacaterPosition.x, CharacaterPosition.y);
            CheckIfWin();
        }

        private void CheckIfWin()
        {
            if(map[CharacaterPosition.y][CharacaterPosition.x] == TerrainType.FINISH)
                gameplayView.ShowWinFeedback();
        }

        private bool IsValidPosition(int x, int y)
        {
            return PositionExistsInMap(x, y) && ThereIsNoTreeInPosition(x, y);
        }

        private bool ThereIsNoTreeInPosition(int x, int y)
        {
            return map[y][x] != TerrainType.TREE;
        }

        private bool PositionExistsInMap(int x, int y)
        {
            return y >= 0 &&
                   y < map.Count && 
                   x >= 0 &&
                   x < map[y].Count;
        }
    }
}