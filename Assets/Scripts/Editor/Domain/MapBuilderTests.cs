using System.Linq;
using Domain;
using NUnit.Framework;

namespace Editor.Domain
{
    public class MapBuilderTests
    {
        private MapBuilder mapBuilder = new MapBuilder();
            
        [Test]
        public void MapCreationIsValid()
        {
            var map = mapBuilder.GenerateMap();
        
            Assert.AreEqual(Configuration.GridWidth, map.Count);
            Assert.AreEqual(Configuration.GridHeight, map[0].Count);
        
            //Hay un solo Start y esta siempre en donde la config indique
            Assert.AreEqual(TerrainType.START, map[Configuration.StartPosition.x][Configuration.StartPosition.y]);
            Assert.AreEqual(1, map.Sum(g => g.Count(cell => cell == TerrainType.START)));

            //Hay un solo Finish y esta siempre en la esquina opuesta al Start
            Assert.AreEqual(TerrainType.FINISH, map[Configuration.GridWidth-1][Configuration.GridHeight-1]);
            Assert.AreEqual(1, map.Sum(g => g.Count(cell => cell == TerrainType.FINISH)));
        }
    }
}