﻿using System.Collections.Generic;
using Delivery;
using Domain;
using NSubstitute;
using NUnit.Framework;
using Presentation;
using UnityEngine;

public class GameplayPresenterTests
{
    private GameplayPresenter gameplayPresenter;
    private MapBuilder mapBuilder;
    private IGameplayView gameplayView;
    
    [SetUp]
    public void SetUp()
    {
        gameplayView = Substitute.For<IGameplayView>();
        mapBuilder = Substitute.For<MapBuilder>();
    }

    [Test]
    public void AsksViewToDrawTheMapOnCreation()
    {
        var dummyMap = new List<List<TerrainType>>();
        dummyMap.Add(new List<TerrainType>{TerrainType.START, TerrainType.GRASS, TerrainType.GRASS});
        mapBuilder.GenerateMap().Returns(dummyMap);
        
        gameplayPresenter = new GameplayPresenter(gameplayView, mapBuilder);
        
        gameplayView.Received(1).InitializeMap(dummyMap, Vector2Int.zero);
    }

    [Test]
    public void CharacterCanMoveRight()
    {
        var dummyMap = new List<List<TerrainType>>();
        dummyMap.Add(new List<TerrainType>{TerrainType.START, TerrainType.GRASS, TerrainType.GRASS});
        mapBuilder.GenerateMap().Returns(dummyMap);
        mapBuilder.GetStartPosition().Returns(new Vector2Int(0, 0));
        
        gameplayPresenter = new GameplayPresenter(gameplayView, mapBuilder);
        
        gameplayPresenter.MoveCharacterRight();
        
        Assert.AreEqual(1, gameplayPresenter.CharacaterPosition.x);
        Assert.AreEqual(0, gameplayPresenter.CharacaterPosition.y);
        gameplayView.Received(1).MovePlayerToCell(1, 0);
    }
    
    [Test]
    public void CharacterCantMoveRightIfAtMapLimit()
    {
        var dummyMap = new List<List<TerrainType>>();
        dummyMap.Add(new List<TerrainType>{TerrainType.GRASS, TerrainType.GRASS, TerrainType.START});
        mapBuilder.GenerateMap().Returns(dummyMap);
        mapBuilder.GetStartPosition().Returns(new Vector2Int(2, 0));
        
        gameplayPresenter = new GameplayPresenter(gameplayView, mapBuilder);
        
        gameplayPresenter.MoveCharacterRight();
        
        Assert.AreEqual(2, gameplayPresenter.CharacaterPosition.x);
        Assert.AreEqual(0, gameplayPresenter.CharacaterPosition.y);
        gameplayView.Received(0).MovePlayerToCell(Arg.Any<int>(), Arg.Any<int>());
    }

    [Test]
    public void CharacterCantMoveRightIfTree()
    {
        var dummyMap = new List<List<TerrainType>>();
        dummyMap.Add(new List<TerrainType>{TerrainType.GRASS, TerrainType.GRASS, TerrainType.START, TerrainType.TREE});
        mapBuilder.GenerateMap().Returns(dummyMap);
        mapBuilder.GetStartPosition().Returns(new Vector2Int(2, 0));
        
        gameplayPresenter = new GameplayPresenter(gameplayView, mapBuilder);
        
        gameplayPresenter.MoveCharacterRight();
        
        Assert.AreEqual(2, gameplayPresenter.CharacaterPosition.x);
        Assert.AreEqual(0, gameplayPresenter.CharacaterPosition.y);
        gameplayView.Received(0).MovePlayerToCell(Arg.Any<int>(), Arg.Any<int>());
    }
    
    [Test]
    public void CharacterCanMoveLeft()
    {
        var dummyMap = new List<List<TerrainType>>();
        dummyMap.Add(new List<TerrainType>{TerrainType.GRASS, TerrainType.GRASS, TerrainType.START});
        mapBuilder.GenerateMap().Returns(dummyMap);
        mapBuilder.GetStartPosition().Returns(new Vector2Int(2, 0));
        
        gameplayPresenter = new GameplayPresenter(gameplayView, mapBuilder);
        
        gameplayPresenter.MoveCharacterLeft();
        
        Assert.AreEqual(1, gameplayPresenter.CharacaterPosition.x);
        Assert.AreEqual(0, gameplayPresenter.CharacaterPosition.y);
        gameplayView.Received(1).MovePlayerToCell(1, 0);
    }
    
    [Test]
    public void CharacterCantMoveLeftIfAtMapLimit()
    {
        var dummyMap = new List<List<TerrainType>>();
        dummyMap.Add(new List<TerrainType>{TerrainType.START, TerrainType.GRASS, TerrainType.GRASS});
        mapBuilder.GenerateMap().Returns(dummyMap);
        mapBuilder.GetStartPosition().Returns(new Vector2Int(0, 0));
        
        gameplayPresenter = new GameplayPresenter(gameplayView, mapBuilder);
        
        gameplayPresenter.MoveCharacterLeft();
        
        Assert.AreEqual(0, gameplayPresenter.CharacaterPosition.x);
        Assert.AreEqual(0, gameplayPresenter.CharacaterPosition.y);
        gameplayView.Received(0).MovePlayerToCell(Arg.Any<int>(), Arg.Any<int>());
    }

    [Test]
    public void CharacterCantMoveLeftIfTree()
    {
        var dummyMap = new List<List<TerrainType>>();
        dummyMap.Add(new List<TerrainType>{TerrainType.GRASS, TerrainType.TREE, TerrainType.START});
        mapBuilder.GenerateMap().Returns(dummyMap);
        mapBuilder.GetStartPosition().Returns(new Vector2Int(2, 0));
        
        gameplayPresenter = new GameplayPresenter(gameplayView, mapBuilder);
        
        gameplayPresenter.MoveCharacterLeft();
        
        Assert.AreEqual(2, gameplayPresenter.CharacaterPosition.x);
        Assert.AreEqual(0, gameplayPresenter.CharacaterPosition.y);
        gameplayView.Received(0).MovePlayerToCell(Arg.Any<int>(), Arg.Any<int>());
    }
    
    [Test]
    public void CharacterCanMoveUp()
    {
        var dummyMap = new List<List<TerrainType>>();
        dummyMap.Add(new List<TerrainType>{TerrainType.START});
        dummyMap.Add(new List<TerrainType>{TerrainType.GRASS});
        dummyMap.Add(new List<TerrainType>{TerrainType.GRASS});
        mapBuilder.GenerateMap().Returns(dummyMap);
        mapBuilder.GetStartPosition().Returns(new Vector2Int(0, 0));
        
        gameplayPresenter = new GameplayPresenter(gameplayView, mapBuilder);
        
        gameplayPresenter.MoveCharacterUp();
        
        Assert.AreEqual(0, gameplayPresenter.CharacaterPosition.x);
        Assert.AreEqual(1, gameplayPresenter.CharacaterPosition.y);
        gameplayView.Received(1).MovePlayerToCell(0, 1);
    }
    
    [Test]
    public void CharacterCantMoveUpIfAtMapLimit()
    {
        var dummyMap = new List<List<TerrainType>>();
        dummyMap.Add(new List<TerrainType>{TerrainType.GRASS});
        dummyMap.Add(new List<TerrainType>{TerrainType.GRASS});
        dummyMap.Add(new List<TerrainType>{TerrainType.START});
        mapBuilder.GenerateMap().Returns(dummyMap);
        mapBuilder.GetStartPosition().Returns(new Vector2Int(0, 2));
        
        gameplayPresenter = new GameplayPresenter(gameplayView, mapBuilder);
        
        gameplayPresenter.MoveCharacterUp();
        
        Assert.AreEqual(0, gameplayPresenter.CharacaterPosition.x);
        Assert.AreEqual(2, gameplayPresenter.CharacaterPosition.y);
        gameplayView.Received(0).MovePlayerToCell(Arg.Any<int>(), Arg.Any<int>());
    }

    [Test]
    public void CharacterCantMoveUpIfTree()
    {
        var dummyMap = new List<List<TerrainType>>();
        dummyMap.Add(new List<TerrainType>{TerrainType.GRASS});
        dummyMap.Add(new List<TerrainType>{TerrainType.GRASS});
        dummyMap.Add(new List<TerrainType>{TerrainType.START});
        dummyMap.Add(new List<TerrainType>{TerrainType.TREE});
        mapBuilder.GenerateMap().Returns(dummyMap);
        mapBuilder.GetStartPosition().Returns(new Vector2Int(0, 2));
        
        gameplayPresenter = new GameplayPresenter(gameplayView, mapBuilder);
        
        gameplayPresenter.MoveCharacterUp();
        
        Assert.AreEqual(0, gameplayPresenter.CharacaterPosition.x);
        Assert.AreEqual(2, gameplayPresenter.CharacaterPosition.y);
        gameplayView.Received(0).MovePlayerToCell(Arg.Any<int>(), Arg.Any<int>());
    }
    
    [Test]
    public void CharacterCanMoveDown()
    {
        var dummyMap = new List<List<TerrainType>>();
        dummyMap.Add(new List<TerrainType>{TerrainType.GRASS});
        dummyMap.Add(new List<TerrainType>{TerrainType.GRASS});
        dummyMap.Add(new List<TerrainType>{TerrainType.START});
        mapBuilder.GenerateMap().Returns(dummyMap);
        mapBuilder.GetStartPosition().Returns(new Vector2Int(0, 2));
        
        gameplayPresenter = new GameplayPresenter(gameplayView, mapBuilder);
        
        gameplayPresenter.MoveCharacterDown();
        
        Assert.AreEqual(0, gameplayPresenter.CharacaterPosition.x);
        Assert.AreEqual(1, gameplayPresenter.CharacaterPosition.y);
        gameplayView.Received(1).MovePlayerToCell(0, 1);
    }
    
    [Test]
    public void CharacterCantMoveDownIfAtMapLimit()
    {
        var dummyMap = new List<List<TerrainType>>();
        dummyMap.Add(new List<TerrainType>{TerrainType.START});
        dummyMap.Add(new List<TerrainType>{TerrainType.GRASS});
        dummyMap.Add(new List<TerrainType>{TerrainType.GRASS});
        mapBuilder.GenerateMap().Returns(dummyMap);
        mapBuilder.GetStartPosition().Returns(new Vector2Int(0, 0));
        
        gameplayPresenter = new GameplayPresenter(gameplayView, mapBuilder);
        
        gameplayPresenter.MoveCharacterDown();
        
        Assert.AreEqual(0, gameplayPresenter.CharacaterPosition.x);
        Assert.AreEqual(0, gameplayPresenter.CharacaterPosition.y);
        gameplayView.Received(0).MovePlayerToCell(Arg.Any<int>(), Arg.Any<int>());
    }

    [Test]
    public void CharacterCantMoveDownIfTree()
    {
        var dummyMap = new List<List<TerrainType>>();
        dummyMap.Add(new List<TerrainType>{TerrainType.TREE});
        dummyMap.Add(new List<TerrainType>{TerrainType.START});
        dummyMap.Add(new List<TerrainType>{TerrainType.GRASS});
        dummyMap.Add(new List<TerrainType>{TerrainType.GRASS});
        mapBuilder.GenerateMap().Returns(dummyMap);
        mapBuilder.GetStartPosition().Returns(new Vector2Int(0, 1));
        
        gameplayPresenter = new GameplayPresenter(gameplayView, mapBuilder);
        
        gameplayPresenter.MoveCharacterDown();
        
        Assert.AreEqual(0, gameplayPresenter.CharacaterPosition.x);
        Assert.AreEqual(1, gameplayPresenter.CharacaterPosition.y);
        gameplayView.Received(0).MovePlayerToCell(Arg.Any<int>(), Arg.Any<int>());
    }

    [Test]
    public void WhenPlayerReachesFinishGameIsWon()
    {
        var dummyMap = new List<List<TerrainType>>();
        dummyMap.Add(new List<TerrainType>{TerrainType.START, TerrainType.FINISH});
        mapBuilder.GenerateMap().Returns(dummyMap);
        mapBuilder.GetStartPosition().Returns(new Vector2Int(0, 0));
        
        gameplayPresenter = new GameplayPresenter(gameplayView, mapBuilder);
        
        gameplayPresenter.MoveCharacterRight();
        gameplayView.Received(1).ShowWinFeedback();
        
    }
    
    [Test]
    public void WhenPlayerNotReachesFinishGameIsNotWon()
    {
        var dummyMap = new List<List<TerrainType>>();
        dummyMap.Add(new List<TerrainType>{TerrainType.START, TerrainType.GRASS, TerrainType.FINISH});
        mapBuilder.GenerateMap().Returns(dummyMap);
        mapBuilder.GetStartPosition().Returns(new Vector2Int(0, 0));
        
        gameplayPresenter = new GameplayPresenter(gameplayView, mapBuilder);
        
        gameplayPresenter.MoveCharacterRight();
        gameplayView.Received(0).ShowWinFeedback();
    }

}
